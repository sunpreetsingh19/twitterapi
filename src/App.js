import React, { Component } from "react";
import "./App.css";
import TwitterContainer from "./imports/TwitterContainer";
class App extends Component {
  state = {
    title: "Donald Trump",
    response: ""
  };

  componentDidMount() {
    this.fetchTweets();
  }

  fetchTweets = () => {
    this.trumpTweets()
      .then(res => this.setState({ response: res }))
      .catch(err => console.log(err));
    this.setState({
      title: "Donald Trump"
    });
  };

  fetchNewTweets = () => {
    this.hillaryTweets()
      .then(res => this.setState({ response: res }))
      .catch(err => console.log(err));
    this.setState({
      title: "Hillary Clinton"
    });
  };
  trumpTweets = async () => {
    const response = await fetch("/search/donald-trump");
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);
    console.log(body);
    return body;
  };

  refreshTweets = () => {
    if (this.state.title === "Donald Trump") {
      this.fetchTweets();
    } else {
      this.fetchNewTweets();
    }
  };

  toggleView = () => {
    if (this.state.title === "Donald Trump") {
      this.fetchNewTweets();
    } else {
      this.fetchTweets();
    }
  };
  hillaryTweets = async () => {
    const response = await fetch(`/search/hillary-clinton`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  renderContainer = () => {
    const { response, title } = this.state;
    return (
      <TwitterContainer
        switchView={this.toggleView}
        refresh={this.refreshTweets}
        tweets={response}
        title={title}
      />
    );
  };
  render() {
    return <div>{this.renderContainer()}</div>;
  }
}
export default App;
