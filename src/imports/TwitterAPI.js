import React from "react";
import Tweet from "./Tweet";

export default class TwitterAPI extends React.Component {
  render() {
    const { tweets } = this.props;
    return tweets.statuses.map(tweet => {
      return (
        <div style={{ width: "70%", margin: "auto" }}>
          <Tweet tweet={tweet} />
        </div>
      );
    });
  }
}
