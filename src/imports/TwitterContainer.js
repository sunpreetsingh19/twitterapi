import React from "react";
import TitleBar from "./TitleBar";
import TwitterAPI from "./TwitterAPI";
export default class TwitterContainer extends React.Component {
  render() {
    const { tweets, switchView, title, refresh } = this.props;
    if (!tweets) {
      return null;
    }
    return (
      <div>
        <TitleBar
            title={title}
          switchView={switchView}
          refresh={refresh}
        />
        <TwitterAPI tweets={tweets} />
      </div>
    );
  }
}
