import React from "react";

export default class Tweet extends React.Component {
  render() {
    const { tweet } = this.props;
    return (
      <div
        style={{
          display: "flex",
          marginBottom: "15px",
          padding: "10px",
          backgroundColor: "#82829045",
          borderRadius: "6px"
        }}
      >
        <div style={{ display: "inline-block", width: "30%" }}>
          <div>
            <img src={tweet.user.profile_image_url} />
          </div>
          <div>{tweet.user.name}</div>
        </div>
        <div style={{ display: "inline-block" }}>{tweet.text}</div>
      </div>
    );
  }
}
