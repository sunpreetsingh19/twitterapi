import React from "react";

export default class TitleBar extends React.Component {
  render() {
    return (
      <div
        style={{
          height: "80px",
          backgroundColor: "#000",
          color: "#fff",
          padding: "15px",
          paddingLeft: "20px",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "15px"
        }}
      >
        <span style={{ fontWeight: "bold", fontSize: "20px" }}>
          Welcome to Twitter
        </span>
        <span
          style={{ fontWeight: "bold", fontSize: "24px", marginTop: "25px" }}
        >
          Now showing {this.props.title}
          's related tweets
        </span>
        <span style={{ textAlign: "right" }}>
          <button onClick={this.props.refresh} className="update-button">
            Update
          </button>
          <button onClick={this.props.switchView} className="update-button">
            Switch
          </button>
        </span>
      </div>
    );
  }
}
